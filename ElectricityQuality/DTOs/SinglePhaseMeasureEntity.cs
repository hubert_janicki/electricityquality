﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityQuality.DTOs
{
    public class SinglePhaseMeasureEntity
    {
        public int Id { get; set; }
        [Description("TimeStamp [ms]")]
        public double TimeStamp { get; set; }
        [Description("Voltage [V]")]
        public double Voltage { get; set; }
        [Description("Current [A]")]
        public double Current { get; set; }

        public SinglePhaseMeasureEntity(int id, double timeStamp, double voltage, double current)
        {
            Id = id;
            TimeStamp = timeStamp;
            Voltage = voltage;
            Current = current;
        }
    }
}
