﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityQuality.DTOs
{
    public class ComponentsEntity
    {
        public double U0 { get; set; }
        public double U1 { get; set; }
        public double U2 { get; set; }
        public double U21p { get; set; }
    }
}
