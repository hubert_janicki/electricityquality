﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectricityQuality.DTOs;

namespace ElectricityQuality.DTOs
{
    public class ThreePhaseMeasure
    {
        public List<SinglePhaseMeasureEntity> L1 { get; set; }
        public List<SinglePhaseMeasureEntity> L2 { get; set; }
        public List<SinglePhaseMeasureEntity> L3 { get; set; }

        public ThreePhaseMeasure(List<SinglePhaseMeasureEntity> l1, List<SinglePhaseMeasureEntity> l2,
            List<SinglePhaseMeasureEntity> l3)
        {
            L1 = l1;
            L2 = l2;
            L3 = l3;
        }
    }
}
