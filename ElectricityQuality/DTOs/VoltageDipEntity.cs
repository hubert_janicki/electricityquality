﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityQuality.DTOs
{
    public class VoltageDipEntity
    {
        public int Id { get; set; }
        public double VoltageFsk { get; set; }
        public int Phase { get; set; }

        public VoltageDipEntity(int id, double voltageFsk, int phase)
        {
            Id = id;
            VoltageFsk = voltageFsk;
            Phase = phase;
        }
    }
}
