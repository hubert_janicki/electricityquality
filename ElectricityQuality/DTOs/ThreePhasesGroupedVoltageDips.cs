﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityQuality.DTOs
{
    public class ThreePhasesGroupedVoltageDips
    {
        public List<List<VoltageDipEntity>> L1 { get; set; }
        public List<List<VoltageDipEntity>> L2 { get; set; }
        public List<List<VoltageDipEntity>> L3 { get; set; }
    }
}
