﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityQuality.DTOs
{
    public class OnePeriodCalculationResult
    {
        public int Id { get; set; }
        public double VoltageFsk { get; set; }
        public double VoltagePhi { get; set; }
        public double CurrentFsk { get; set; }
        public double CurrentPhi { get; set; }
        public double Power { get; set; }
        public double TanPhi { get; set; }
    }
}
