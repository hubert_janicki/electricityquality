﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectricityQuality.DTOs;

namespace ElectricityQuality.Calculators
{
    public class SinglePhaseMeasuresCalculator : MeasuresCalculator
    {
        private readonly List<SinglePhaseMeasureEntity> _singlePhaseMeasureEntities;
        private readonly int _averagePeriod;

        public SinglePhaseMeasuresCalculator(List<SinglePhaseMeasureEntity> singlePhaseMeasureEntities, int averagePeriod) : base(averagePeriod, 12)
        {
            _singlePhaseMeasureEntities = singlePhaseMeasureEntities ??
                                          throw new ArgumentNullException(nameof(singlePhaseMeasureEntities));

            _averagePeriod = averagePeriod;
        }

        public override List<List<OnePeriodCalculationResult>> Calculate()
        {
            Log.AddMessage(@"Rozpoczeto obliczanie");
            var onePhaseResultsList = new List<List<OnePeriodCalculationResult>>
            {
                DoCalculationWork(_singlePhaseMeasureEntities)
            };

            Log.AddMessage(@"Zakonczono obliczanie");

            return onePhaseResultsList;
        }

        public override void WriteResultsToFile(List<List<OnePeriodCalculationResult>> onePeriodCalculationResults, string path)
        {
            var doubleFormatSpecifier = "F";
            Log.AddMessage(@"Rozpoczeto zapis do pliku");
            using (var fileWriter = new StreamWriter(path))
            {
                fileWriter.WriteLine($"Id;U;I;P;TgPhi");
                foreach (var onePeriodCalculationResult in onePeriodCalculationResults)
                {
                    foreach (var calculationResult in onePeriodCalculationResult)
                    {
                        fileWriter.WriteLine($"{calculationResult.Id.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA"))};" +
                                             $"{calculationResult.VoltageFsk.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA"))};" +
                                             $"{calculationResult.CurrentFsk.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA"))};" +
                                             $"{calculationResult.Power.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA"))};" +
                                             $"{calculationResult.TanPhi.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA"))}");
                    }
                }
            }
            Log.AddMessage(@"Zakonczono zapis do pliku");
        }
    }
}
