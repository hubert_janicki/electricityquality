﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectricityQuality.DTOs;

namespace ElectricityQuality.Calculators
{
    public class ThreePhaseMeasuresCalculator : MeasuresCalculator
    {
        private readonly ThreePhaseMeasure _threePhaseMeasureEntities;
        private readonly int _averagePeriod;

        public ThreePhaseMeasuresCalculator(ThreePhaseMeasure threePhaseMeasureEntities, int averagePeriod) : base(averagePeriod, 12)
        {
            _threePhaseMeasureEntities = threePhaseMeasureEntities ??
                                         throw new ArgumentNullException(nameof(threePhaseMeasureEntities));

            _averagePeriod = averagePeriod;
        }

        public override List<List<OnePeriodCalculationResult>> Calculate()
        {
            Log.AddMessage(@"Rozpoczeto obliczanie");
            var threePhasesResultsList = new List<List<OnePeriodCalculationResult>>
            {
                DoCalculationWork(_threePhaseMeasureEntities.L1),
                DoCalculationWork(_threePhaseMeasureEntities.L2),
                DoCalculationWork(_threePhaseMeasureEntities.L3)
            };

            Log.AddMessage(@"Zakonczono obliczanie");

            return threePhasesResultsList;
        }

        public override void WriteResultsToFile(List<List<OnePeriodCalculationResult>> onePeriodCalculationResults, string path)
        {
            Log.AddMessage(@"Rozpoczeto zapis do pliku");
            //var pattern = $"Id;U_L1;PhiUL1;I_L1;PhiIL1;PL1;TgPhiL1;U_L2;PhiUL2;I_L2;PhiIL2;PL2;TgPhiL2;U_L3;PhiUL3;I_L3;PhiIL3;PL3;TgPhiL3";
            var pattern = $"Id;U_L1;I_L1;PL1;TgPhiL1;U_L2;I_L2;;PL2;TgPhiL2;U_L3;I_L3;PL3;TgPhiL3;U0;U1;U2;U21%";
            var doubleFormatSpecifier = "F";

            var lines = Enumerable.Repeat(pattern, onePeriodCalculationResults[0].Count).ToList();

            var phase = 1;
            foreach (var onePeriodCalculationResult in onePeriodCalculationResults)
            {
                for (var i = 0; i < onePeriodCalculationResult.Count; i++)
                {
                    lines[i] = lines[i].Replace($"Id", onePeriodCalculationResult[i].Id.ToString(CultureInfo.InvariantCulture));
                    lines[i] = lines[i].Replace($"U_L{phase}",onePeriodCalculationResult[i].VoltageFsk.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA")));
                    lines[i] = lines[i].Replace($"I_L{phase}", onePeriodCalculationResult[i].CurrentFsk.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA")));
                    lines[i] = lines[i].Replace($"PL{phase}", onePeriodCalculationResult[i].Power.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA")));
                    lines[i] = lines[i].Replace($"TgPhiL{phase}", onePeriodCalculationResult[i].TanPhi.ToString(doubleFormatSpecifier,CultureInfo.CreateSpecificCulture("en-CA")));
                }
                phase++;
            }

            var componentsEntities = GetComponentsEntities(onePeriodCalculationResults);

            for (var i = 0; i < onePeriodCalculationResults[0].Count; i++)
            {
                lines[i] = lines[i].Replace($"U0", componentsEntities[i].U0.ToString(doubleFormatSpecifier, CultureInfo.CreateSpecificCulture("en-CA")));
                lines[i] = lines[i].Replace($"U1", componentsEntities[i].U1.ToString(doubleFormatSpecifier, CultureInfo.CreateSpecificCulture("en-CA")));
                lines[i] = lines[i].Replace($"U2", componentsEntities[i].U2.ToString(doubleFormatSpecifier, CultureInfo.CreateSpecificCulture("en-CA")));
                lines[i] = lines[i].Replace($"U21%", componentsEntities[i].U21p.ToString(doubleFormatSpecifier, CultureInfo.CreateSpecificCulture("en-CA")));
            }

            using (var fileWriter = new StreamWriter(path))
            {
                fileWriter.WriteLine(pattern);

                foreach (var line in lines)
                {
                    fileWriter.WriteLine(line);
                }
            }
            Log.AddMessage(@"Zakonczono zapis do pliku");
        }

        private List<ComponentsEntity> GetComponentsEntities(List<List<OnePeriodCalculationResult>> onePeriodCalculationResults)
        {
            var componentsEntities = new List<ComponentsEntity>();
            var L1 = onePeriodCalculationResults[0];
            var L2 = onePeriodCalculationResults[1];
            var L3 = onePeriodCalculationResults[2];

            var radians120 = 120 * Math.PI / 180;
            var radians240 = 240 * Math.PI / 180;

            for (var i = 0; i < L1.Count; i++)
            {
                var U0 = Math.Sqrt(Math.Pow(L1[i].VoltageFsk * Math.Sin(L1[i].VoltagePhi) + L2[i].VoltageFsk * Math.Sin(L2[i].VoltagePhi) + L3[i].VoltageFsk * Math.Sin(L3[i].VoltagePhi),2) +
                                   Math.Pow(L1[i].VoltageFsk * Math.Cos(L1[i].VoltagePhi) + L2[i].VoltageFsk * Math.Cos(L2[i].VoltagePhi) + L3[i].VoltageFsk * Math.Cos(L3[i].VoltagePhi), 2)) 
                                    / 3;
                var U1 = Math.Sqrt(Math.Pow(L1[i].VoltageFsk * Math.Sin(L1[i].VoltagePhi) + L2[i].VoltageFsk * Math.Sin(L2[i].VoltagePhi + radians120) + L3[i].VoltageFsk * Math.Sin(L3[i].VoltagePhi + radians240), 2) +
                                   Math.Pow(L1[i].VoltageFsk * Math.Cos(L1[i].VoltagePhi) + L2[i].VoltageFsk * Math.Cos(L2[i].VoltagePhi + radians240) + L3[i].VoltageFsk * Math.Cos(L3[i].VoltagePhi + radians120), 2))
                                / 3;
                var U2 = Math.Sqrt(Math.Pow(L1[i].VoltageFsk * Math.Sin(L1[i].VoltagePhi) + L2[i].VoltageFsk * Math.Sin(L2[i].VoltagePhi + radians240) + L3[i].VoltageFsk * Math.Sin(L3[i].VoltagePhi + radians120), 2) +
                                   Math.Pow(L1[i].VoltageFsk * Math.Cos(L1[i].VoltagePhi) + L2[i].VoltageFsk * Math.Cos(L2[i].VoltagePhi + radians120) + L3[i].VoltageFsk * Math.Cos(L3[i].VoltagePhi + radians240), 2))
                            / 3;

                var U21p = 100 * U2 / U1;

                componentsEntities.Add(new ComponentsEntity()
                {
                    U0 = U0,
                    U1 = U1,
                    U2 = U2,
                    U21p = U21p
                });
            }

            return componentsEntities;
        }
    }
}
