﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace ElectricityQuality.DTOs
{
    public abstract class MeasuresCalculator
    {
        private readonly int _averagePeriod;
        private readonly int _entitiesChunkSize;

        private List<VoltageDipEntity> _voltageDipEntities;

        public abstract void WriteResultsToFile(List<List<OnePeriodCalculationResult>> onePeriodCalculationResults, string path);
        public abstract List<List<OnePeriodCalculationResult>> Calculate();

        protected MeasuresCalculator(int averagePeriod, int entitiesChunkSize)
        {
            _averagePeriod = averagePeriod;
            _entitiesChunkSize = entitiesChunkSize;

            _voltageDipEntities = new List<VoltageDipEntity>();
        }

        protected List<OnePeriodCalculationResult> DoCalculationWork(List<SinglePhaseMeasureEntity> singlePhaseMeasureEntities)
        {
            var measuresPreparedForCalculation = GetPreparedMeasuresForCalculation(singlePhaseMeasureEntities);
            var voltageFskFinalList = new List<double>();
            var voltagePhiFinalList = new List<double>();
            var currentFskFinalList = new List<double>();
            var currentPhiFinalList = new List<double>();

            foreach (var measurePreparedForCalculation in measuresPreparedForCalculation)
            {
                var voltageFskList = new List<double>();
                var voltagePhiList = new List<double>();
                var currentFskList = new List<double>();
                var currentPhiList = new List<double>();

                foreach (var entitiesChunk in measurePreparedForCalculation.Value)
                {
                    var voltageA = 0.0d;
                    var voltageB = 0.0d;
                    var voltagePhi = 0.0d;

                    var currentA = 0.0d;
                    var currentB = 0.0d;
                    var currentPhi = 0.0d;

                    var measuresCount = entitiesChunk.Count;
                    foreach (var entity in entitiesChunk)
                    {
                        var k = entitiesChunk.IndexOf(entity) + 1;
                        var x = (double) (2 * Math.PI * k) / (double) (measuresCount);

                        var sin = Math.Sin(x);
                        var cos = Math.Cos(x);

                        voltageA += entity.Voltage * cos;
                        voltageB += entity.Voltage * sin;

                        currentA += entity.Current * cos;
                        currentB += entity.Current * sin;
                    }

                    var constant = (double) 2 / measuresCount;
                    voltageA *= constant;
                    voltageB *= constant;

                    currentA *= constant;
                    currentB *= constant;

                    var voltageFsk = Math.Sqrt(Math.Pow(voltageA, 2) + Math.Pow(voltageB, 2)) / Math.Sqrt(2);
                    var currentFsk = Math.Sqrt(Math.Pow(currentA, 2) + Math.Pow(currentB, 2)) / Math.Sqrt(2);

                    if (voltageA.Equals(0.0d))
                    {
                        voltagePhi = voltageB > 0 ? Math.PI / 2 : -Math.PI / 2;
                    }
                    else
                    {
                        voltagePhi = voltageA > 0
                            ? Acot(voltageB / voltageA)
                            : Math.PI + Acot(voltageB / voltageA);
                    }

                    if (currentA.Equals(0.0d))
                    {
                        currentPhi = currentB > 0 ? Math.PI / 2 : - Math.PI / 2;
                    }
                    else
                    {
                        currentPhi = currentB > 0
                            ? Acot(currentB / currentA)
                            : Math.PI + Acot(currentB / currentA);
                    }

                    voltageFskList.Add(voltageFsk);
                    voltagePhiList.Add(voltagePhi);
                    currentFskList.Add(currentFsk);
                    currentPhiList.Add(currentPhi);
                }
                
                voltageFskFinalList.Add(voltageFskList.Average());
                voltagePhiFinalList.Add(voltagePhiList.Average());
                currentFskFinalList.Add(currentFskList.Average());
                currentPhiFinalList.Add(currentPhiList.Average());
            }

            var calculationResultsList = new List<OnePeriodCalculationResult>();

            for (var i = 0; i < measuresPreparedForCalculation.Count; i++)
            {
                var phiL = voltagePhiFinalList[i] - currentPhiFinalList[i];
                calculationResultsList.Add(new OnePeriodCalculationResult
                {
                    Id = i + 1,
                    VoltageFsk = voltageFskFinalList[i],
                    VoltagePhi = voltagePhiFinalList[i],
                    CurrentFsk = currentFskFinalList[i],
                    CurrentPhi = currentPhiFinalList[i],
                    Power = voltageFskFinalList[i] * currentFskFinalList[i] * Math.Cos(phiL),
                    TanPhi = Math.Tan(phiL)
                });
            }

            return calculationResultsList;
        }

        private Dictionary<int, List<List<SinglePhaseMeasureEntity>>> GetPreparedMeasuresForCalculation(
            List<SinglePhaseMeasureEntity> singlePhaseMeasureEntities)
        {
            var measuresForPeriods = ChunkMeasureEntitiesForPeriods(singlePhaseMeasureEntities, _averagePeriod).ToList();

            var chunkedMeasuresForPeriod = measuresForPeriods.ToDictionary(
                measuresForPeriod => measuresForPeriod.Key,
                measuresForPeriod => ChunkMeasureEntities(measuresForPeriod.Value, _entitiesChunkSize).ToList());

            return chunkedMeasuresForPeriod;
        }

        private Dictionary<int, List<SinglePhaseMeasureEntity>> ChunkMeasureEntitiesForPeriods(List<SinglePhaseMeasureEntity> singlePhaseMeasureEntities, int period)
        {
            var chunks = new Dictionary<int, List<SinglePhaseMeasureEntity>>();

            var startTimestamp = singlePhaseMeasureEntities[0].TimeStamp;
            var stopTimestamp =
                startTimestamp + period < singlePhaseMeasureEntities[singlePhaseMeasureEntities.Count - 1].TimeStamp
                    ? startTimestamp + period
                    : singlePhaseMeasureEntities[singlePhaseMeasureEntities.Count - 1].TimeStamp;

            var key = 0;
            foreach (var singlePhaseMeasureEntity in singlePhaseMeasureEntities)
            {
                if (singlePhaseMeasureEntity.TimeStamp < stopTimestamp)
                    continue;

                var periodEntities = singlePhaseMeasureEntities.Where(
                    entity => entity.TimeStamp >= startTimestamp && entity.TimeStamp < stopTimestamp).ToList();

                if(periodEntities.Count == 0)
                    continue;

                chunks.Add(key++, periodEntities);

                startTimestamp = singlePhaseMeasureEntity.TimeStamp;
                stopTimestamp =
                    startTimestamp + period < singlePhaseMeasureEntities[singlePhaseMeasureEntities.Count - 1].TimeStamp
                        ? startTimestamp + period
                        : singlePhaseMeasureEntities[singlePhaseMeasureEntities.Count - 1].TimeStamp - 1;
            }

            return chunks;
        }

        private IEnumerable<List<SinglePhaseMeasureEntity>> ChunkMeasureEntities(List<SinglePhaseMeasureEntity> singlePhaseMeasureEntities, int chunkSize)
        {
            //for (var i = 0; i < singlePhaseMeasureEntities.Count - chunkSize; i++)
            //{
            //    yield return singlePhaseMeasureEntities.GetRange(i,
            //        Math.Min(chunkSize, singlePhaseMeasureEntities.Count - i));
            //}

            for (var i = 0; i < singlePhaseMeasureEntities.Count; i += chunkSize)
            {
                yield return singlePhaseMeasureEntities.GetRange(i,
                    Math.Min(chunkSize, singlePhaseMeasureEntities.Count - i));
            }
        }

        public static double Acot(double x)
        {
            return Math.PI / 2 - Math.Atan(x);

            //var angle = Math.Atan2(y, x);

            //if (angle < 0)
            //    angle += 2 * Math.PI;

            //angle = Math.PI / 2 - angle;

            //if (angle < 0)
            //    angle += 2 * Math.PI;

            //return angle;
        }
    }
}
