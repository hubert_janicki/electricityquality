﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectricityQuality.DTOs;

namespace ElectricityQuality.Calculators
{
    public class VoltageDipsCalculator
    {
        private readonly List<List<OnePeriodCalculationResult>> _onePeriodCalculationResults;
        private readonly double _voltageDipTriggerValue;
        public VoltageDipsCalculator(List<List<OnePeriodCalculationResult>> onePeriodCalculationResults)
        {
            _onePeriodCalculationResults = onePeriodCalculationResults ?? throw new ArgumentNullException(nameof(onePeriodCalculationResults));

            _voltageDipTriggerValue = double.Parse(ConfigurationManager.AppSettings["VoltageDipTriggerValue"], CultureInfo.InvariantCulture);
        }

        public ThreePhasesGroupedVoltageDips Calculate()
        {
            var temporaryVoltageDipsList = new List<List<VoltageDipEntity>>();

            for (var i = 0; i < _onePeriodCalculationResults.Count; i++)
            {
                var onePhaseVoltageDipsList = new List<VoltageDipEntity>();
                foreach (var calculationResult in _onePeriodCalculationResults[i])
                {
                    if (calculationResult.VoltageFsk < _voltageDipTriggerValue)
                        onePhaseVoltageDipsList.Add(
                            new VoltageDipEntity(calculationResult.Id, calculationResult.VoltageFsk, i+1)
                        );
                }
                temporaryVoltageDipsList.Add(onePhaseVoltageDipsList);
            }

            var groupedVoltageDips = GroupDipsForPhases(temporaryVoltageDipsList);

            return groupedVoltageDips;
        }

        public void WriteToFile(ThreePhasesGroupedVoltageDips threePhasesVoltageDips, string path, int averagePeriod)
        {
            var lines = new List<string>();
            averagePeriod /= 1000; //for seconds

            if (threePhasesVoltageDips.L1.Count > 0)
            {
                lines.Add("L1\n");

                var voltageDipNumber = 1;
                foreach (var voltageDipsGroup in threePhasesVoltageDips.L1)
                {
                    var dipStartTimestamp = voltageDipsGroup.Min(dip => dip.Id) * averagePeriod - averagePeriod;
                    var dipEndTimestamp = voltageDipsGroup.Max(dip => dip.Id) * averagePeriod;

                    lines.Add($"Zapad {voltageDipNumber++}");
                    lines.Add($"Czas rozpoczecia: {dipStartTimestamp}");
                    lines.Add($"Czas zakonczenia: {dipEndTimestamp}");
                    lines.Add($"Czas trwania: {dipEndTimestamp - dipStartTimestamp}");
                    lines.Add($"Wartosc srednia napiecia podczas zapadu: {voltageDipsGroup.Average(dip => dip.VoltageFsk)}");
                    lines.Add("\n");
                }
            }

            if (threePhasesVoltageDips.L2.Count > 0)
            {
                lines.Add("L2\n");

                var voltageDipNumber = 1;
                foreach (var voltageDipsGroup in threePhasesVoltageDips.L1)
                {
                    var dipStartTimestamp = voltageDipsGroup.Min(dip => dip.Id) * averagePeriod - averagePeriod;
                    var dipEndTimestamp = voltageDipsGroup.Max(dip => dip.Id) * averagePeriod;

                    lines.Add($"Zapad {voltageDipNumber++}");
                    lines.Add($"Czas rozpoczecia: {dipStartTimestamp}");
                    lines.Add($"Czas zakonczenia: {dipEndTimestamp}");
                    lines.Add($"Czas trwania: {dipEndTimestamp - dipStartTimestamp}");
                    lines.Add($"Wartosc srednia napiecia podczas zapadu: {voltageDipsGroup.Average(dip => dip.VoltageFsk)}");
                    lines.Add("\n");
                }
            }

            if (threePhasesVoltageDips.L3.Count > 0)
            {
                lines.Add("L3\n");

                var voltageDipNumber = 1;
                foreach (var voltageDipsGroup in threePhasesVoltageDips.L1)
                {
                    var dipStartTimestamp = voltageDipsGroup.Min(dip => dip.Id) * averagePeriod - averagePeriod;
                    var dipEndTimestamp = voltageDipsGroup.Max(dip => dip.Id) * averagePeriod;

                    lines.Add($"Zapad {voltageDipNumber++}");
                    lines.Add($"Czas rozpoczecia: {dipStartTimestamp}");
                    lines.Add($"Czas zakonczenia: {dipEndTimestamp}");
                    lines.Add($"Czas trwania: {dipEndTimestamp - dipStartTimestamp}");
                    lines.Add($"Wartosc srednia napiecia podczas zapadu: {voltageDipsGroup.Average(dip => dip.VoltageFsk)}");
                    lines.Add("\n");
                }
            }



            using (var fileWriter = new StreamWriter(path))
            {
                foreach (var line in lines)
                {
                    fileWriter.WriteLine(line);
                }
            }
        }

        private ThreePhasesGroupedVoltageDips GroupDipsForPhases(List<List<VoltageDipEntity>> threePhasesVoltageDips)
        {
            var L1 = new List<List<VoltageDipEntity>>();
            var dipsGroupL1 = new List<VoltageDipEntity>();

            for (var i = 0; i < threePhasesVoltageDips[0].Count; i++)
            {
                if (dipsGroupL1.Count == 0)
                {
                    dipsGroupL1.Add(threePhasesVoltageDips[0][i]);
                }

                if (dipsGroupL1.Count != 0 && i + 1 != threePhasesVoltageDips[0].Count)
                {
                    if (threePhasesVoltageDips[0][i + 1].Id == threePhasesVoltageDips[0][i].Id + 1)
                    {
                        dipsGroupL1.Add(threePhasesVoltageDips[0][i]);
                    }
                    else
                    {
                        L1.Add(dipsGroupL1.ToList());

                        dipsGroupL1 = new List<VoltageDipEntity>();
                    }
                }
                else
                {
                    L1.Add(dipsGroupL1.ToList());

                    dipsGroupL1 = new List<VoltageDipEntity>();
                }
            }

            var L2 = new List<List<VoltageDipEntity>>();
            var dipsGroupL2 = new List<VoltageDipEntity>();

            for (var i = 0; i < threePhasesVoltageDips[1].Count; i++)
            {
                if (dipsGroupL2.Count == 0)
                {
                    dipsGroupL2.Add(threePhasesVoltageDips[0][i]);
                }

                if (dipsGroupL2.Count != 0 && i + 1 != threePhasesVoltageDips[0].Count)
                {
                    if (threePhasesVoltageDips[0][i + 1].Id == threePhasesVoltageDips[0][i].Id + 1)
                    {
                        dipsGroupL2.Add(threePhasesVoltageDips[0][i]);
                    }
                    else
                    {
                        L2.Add(dipsGroupL2.ToList());

                        dipsGroupL2 = new List<VoltageDipEntity>();
                    }
                }
                else
                {
                    L2.Add(dipsGroupL2.ToList());

                    dipsGroupL2 = new List<VoltageDipEntity>();
                }
            }

            var L3 = new List<List<VoltageDipEntity>>();
            var dipsGroupL3 = new List<VoltageDipEntity>();

            for (var i = 0; i < threePhasesVoltageDips[2].Count; i++)
            {
                if (dipsGroupL3.Count == 0)
                {
                    dipsGroupL3.Add(threePhasesVoltageDips[0][i]);
                }

                if (dipsGroupL3.Count != 0 && i + 1 != threePhasesVoltageDips[0].Count)
                {
                    if (threePhasesVoltageDips[0][i + 1].Id == threePhasesVoltageDips[0][i].Id + 1)
                    {
                        dipsGroupL3.Add(threePhasesVoltageDips[0][i]);
                    }
                    else
                    {
                        L3.Add(dipsGroupL3.ToList());

                        dipsGroupL3 = new List<VoltageDipEntity>();
                    }
                }
                else
                {
                    L3.Add(dipsGroupL3.ToList());

                    dipsGroupL3 = new List<VoltageDipEntity>();
                }
            }

            return new ThreePhasesGroupedVoltageDips()
            {
                L1 = L1,
                L2 = L2,
                L3 = L3
            };
        }
    }
}
