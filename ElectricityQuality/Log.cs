﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElectricityQuality.Views;

namespace ElectricityQuality
{
    public class Log
    {
        private static TextBox _logTextBox;

        public Log(TextBox logTextBox)
        {
            _logTextBox = logTextBox;
        }

        public static void AddMessage(string message)
        {
            var date = DateTime.Now;

            var formattedMessage = $"[{date.ToString("HH:mm:ss")}] {message}\n";

            _logTextBox.Text += formattedMessage;
        }
    }
}
