﻿namespace ElectricityQuality.Views
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BUT_loadFile = new System.Windows.Forms.Button();
            this.TXT_fileName = new System.Windows.Forms.TextBox();
            this.BUT_goToCalculationsWindow = new System.Windows.Forms.Button();
            this.TXT_log = new System.Windows.Forms.TextBox();
            this.LBL_log = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BUT_loadFile
            // 
            this.BUT_loadFile.Location = new System.Drawing.Point(226, 12);
            this.BUT_loadFile.Name = "BUT_loadFile";
            this.BUT_loadFile.Size = new System.Drawing.Size(96, 44);
            this.BUT_loadFile.TabIndex = 0;
            this.BUT_loadFile.Text = "Wczytaj plik";
            this.BUT_loadFile.UseVisualStyleBackColor = true;
            this.BUT_loadFile.Click += new System.EventHandler(this.BUT_loadFile_Click);
            // 
            // TXT_fileName
            // 
            this.TXT_fileName.Location = new System.Drawing.Point(13, 25);
            this.TXT_fileName.Name = "TXT_fileName";
            this.TXT_fileName.Size = new System.Drawing.Size(202, 20);
            this.TXT_fileName.TabIndex = 1;
            // 
            // BUT_goToCalculationsWindow
            // 
            this.BUT_goToCalculationsWindow.Enabled = false;
            this.BUT_goToCalculationsWindow.Location = new System.Drawing.Point(107, 108);
            this.BUT_goToCalculationsWindow.Name = "BUT_goToCalculationsWindow";
            this.BUT_goToCalculationsWindow.Size = new System.Drawing.Size(137, 44);
            this.BUT_goToCalculationsWindow.TabIndex = 2;
            this.BUT_goToCalculationsWindow.Text = "Przejdź do obliczeń";
            this.BUT_goToCalculationsWindow.UseVisualStyleBackColor = true;
            this.BUT_goToCalculationsWindow.Click += new System.EventHandler(this.BUT_goToCalculationsWindow_Click);
            // 
            // TXT_log
            // 
            this.TXT_log.Location = new System.Drawing.Point(13, 210);
            this.TXT_log.Multiline = true;
            this.TXT_log.Name = "TXT_log";
            this.TXT_log.Size = new System.Drawing.Size(309, 174);
            this.TXT_log.TabIndex = 3;
            // 
            // LBL_log
            // 
            this.LBL_log.AutoSize = true;
            this.LBL_log.Location = new System.Drawing.Point(13, 191);
            this.LBL_log.Name = "LBL_log";
            this.LBL_log.Size = new System.Drawing.Size(25, 13);
            this.LBL_log.TabIndex = 4;
            this.LBL_log.Text = "Log";
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 396);
            this.Controls.Add(this.LBL_log);
            this.Controls.Add(this.TXT_log);
            this.Controls.Add(this.BUT_goToCalculationsWindow);
            this.Controls.Add(this.TXT_fileName);
            this.Controls.Add(this.BUT_loadFile);
            this.Name = "MainView";
            this.Text = "MainWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BUT_loadFile;
        private System.Windows.Forms.TextBox TXT_fileName;
        private System.Windows.Forms.Button BUT_goToCalculationsWindow;
        private System.Windows.Forms.Label LBL_log;
        private System.Windows.Forms.TextBox TXT_log;
    }
}

