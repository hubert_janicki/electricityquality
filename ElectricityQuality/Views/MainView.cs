﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElectricityQuality.Views
{
    public partial class MainView : Form
    {
        private CalculationsView _calculationsView;
        private string _measureFilePath;
        public MainView()
        {
            InitializeComponent();
            InitializeLogging();

            Log.AddMessage("Start");
        }

        private void InitializeLogging()
        {
            var log = new Log(TXT_log);
        }

        private void BUT_loadFile_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog()
            {
                Filter = @"txt files (*.txt)|*.txt|All files (*.*)|*.*"
            };
            openFileDialog.ShowDialog();

            TXT_fileName.Text = openFileDialog.SafeFileName;
            _measureFilePath = openFileDialog.FileName;

            BUT_goToCalculationsWindow.Enabled = true;
        }

        private void BUT_goToCalculationsWindow_Click(object sender, EventArgs e)
        {
            _calculationsView = new CalculationsView(_measureFilePath);
            _calculationsView.ShowDialog();
        }
    }
}
