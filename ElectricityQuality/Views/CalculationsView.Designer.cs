﻿namespace ElectricityQuality.Views
{
    partial class CalculationsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PNL_measureConfiguration = new System.Windows.Forms.Panel();
            this.RB_threePhaseMeasure = new System.Windows.Forms.RadioButton();
            this.RB_singlePhaseMeasure = new System.Windows.Forms.RadioButton();
            this.LBL_chooseMeasureConfiguration = new System.Windows.Forms.Label();
            this.BUT_calculate = new System.Windows.Forms.Button();
            this.PNL_averagingPeriod = new System.Windows.Forms.Panel();
            this.RB_10mins = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.RB_1min = new System.Windows.Forms.RadioButton();
            this.RB_10sec = new System.Windows.Forms.RadioButton();
            this.PNL_measureConfiguration.SuspendLayout();
            this.PNL_averagingPeriod.SuspendLayout();
            this.SuspendLayout();
            // 
            // PNL_measureConfiguration
            // 
            this.PNL_measureConfiguration.Controls.Add(this.RB_threePhaseMeasure);
            this.PNL_measureConfiguration.Controls.Add(this.RB_singlePhaseMeasure);
            this.PNL_measureConfiguration.Controls.Add(this.LBL_chooseMeasureConfiguration);
            this.PNL_measureConfiguration.Location = new System.Drawing.Point(12, 23);
            this.PNL_measureConfiguration.Name = "PNL_measureConfiguration";
            this.PNL_measureConfiguration.Size = new System.Drawing.Size(299, 126);
            this.PNL_measureConfiguration.TabIndex = 0;
            // 
            // RB_threePhaseMeasure
            // 
            this.RB_threePhaseMeasure.AutoSize = true;
            this.RB_threePhaseMeasure.Location = new System.Drawing.Point(92, 77);
            this.RB_threePhaseMeasure.Name = "RB_threePhaseMeasure";
            this.RB_threePhaseMeasure.Size = new System.Drawing.Size(107, 17);
            this.RB_threePhaseMeasure.TabIndex = 2;
            this.RB_threePhaseMeasure.TabStop = true;
            this.RB_threePhaseMeasure.Text = "Pomiar trójfazowy";
            this.RB_threePhaseMeasure.UseVisualStyleBackColor = true;
            // 
            // RB_singlePhaseMeasure
            // 
            this.RB_singlePhaseMeasure.AutoSize = true;
            this.RB_singlePhaseMeasure.Checked = true;
            this.RB_singlePhaseMeasure.Location = new System.Drawing.Point(92, 43);
            this.RB_singlePhaseMeasure.Name = "RB_singlePhaseMeasure";
            this.RB_singlePhaseMeasure.Size = new System.Drawing.Size(119, 17);
            this.RB_singlePhaseMeasure.TabIndex = 1;
            this.RB_singlePhaseMeasure.TabStop = true;
            this.RB_singlePhaseMeasure.Text = "Pomiar jednofazowy";
            this.RB_singlePhaseMeasure.UseVisualStyleBackColor = true;
            // 
            // LBL_chooseMeasureConfiguration
            // 
            this.LBL_chooseMeasureConfiguration.AutoSize = true;
            this.LBL_chooseMeasureConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.LBL_chooseMeasureConfiguration.Location = new System.Drawing.Point(58, 13);
            this.LBL_chooseMeasureConfiguration.Name = "LBL_chooseMeasureConfiguration";
            this.LBL_chooseMeasureConfiguration.Size = new System.Drawing.Size(195, 17);
            this.LBL_chooseMeasureConfiguration.TabIndex = 0;
            this.LBL_chooseMeasureConfiguration.Text = "Wybierz konfigurację pomiaru";
            // 
            // BUT_calculate
            // 
            this.BUT_calculate.Location = new System.Drawing.Point(95, 330);
            this.BUT_calculate.Name = "BUT_calculate";
            this.BUT_calculate.Size = new System.Drawing.Size(137, 44);
            this.BUT_calculate.TabIndex = 3;
            this.BUT_calculate.Text = "Oblicz";
            this.BUT_calculate.UseVisualStyleBackColor = true;
            this.BUT_calculate.Click += new System.EventHandler(this.BUT_calculate_Click);
            // 
            // PNL_averagingPeriod
            // 
            this.PNL_averagingPeriod.Controls.Add(this.RB_10sec);
            this.PNL_averagingPeriod.Controls.Add(this.RB_1min);
            this.PNL_averagingPeriod.Controls.Add(this.RB_10mins);
            this.PNL_averagingPeriod.Controls.Add(this.label1);
            this.PNL_averagingPeriod.Location = new System.Drawing.Point(12, 164);
            this.PNL_averagingPeriod.Name = "PNL_averagingPeriod";
            this.PNL_averagingPeriod.Size = new System.Drawing.Size(299, 126);
            this.PNL_averagingPeriod.TabIndex = 3;
            // 
            // RB_10mins
            // 
            this.RB_10mins.AutoSize = true;
            this.RB_10mins.Checked = true;
            this.RB_10mins.Location = new System.Drawing.Point(18, 54);
            this.RB_10mins.Name = "RB_10mins";
            this.RB_10mins.Size = new System.Drawing.Size(59, 17);
            this.RB_10mins.TabIndex = 1;
            this.RB_10mins.TabStop = true;
            this.RB_10mins.Text = "10 min ";
            this.RB_10mins.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(58, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wybierz konfigurację pomiaru";
            // 
            // RB_1min
            // 
            this.RB_1min.AutoSize = true;
            this.RB_1min.Location = new System.Drawing.Point(120, 55);
            this.RB_1min.Name = "RB_1min";
            this.RB_1min.Size = new System.Drawing.Size(53, 17);
            this.RB_1min.TabIndex = 2;
            this.RB_1min.TabStop = true;
            this.RB_1min.Text = "1 min ";
            this.RB_1min.UseVisualStyleBackColor = true;
            // 
            // RB_10sec
            // 
            this.RB_10sec.AutoSize = true;
            this.RB_10sec.Location = new System.Drawing.Point(222, 54);
            this.RB_10sec.Name = "RB_10sec";
            this.RB_10sec.Size = new System.Drawing.Size(57, 17);
            this.RB_10sec.TabIndex = 3;
            this.RB_10sec.TabStop = true;
            this.RB_10sec.Text = "10 sek";
            this.RB_10sec.UseVisualStyleBackColor = true;
            // 
            // CalculationsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 445);
            this.Controls.Add(this.PNL_averagingPeriod);
            this.Controls.Add(this.BUT_calculate);
            this.Controls.Add(this.PNL_measureConfiguration);
            this.Name = "CalculationsView";
            this.Text = "CalculationsView";
            this.PNL_measureConfiguration.ResumeLayout(false);
            this.PNL_measureConfiguration.PerformLayout();
            this.PNL_averagingPeriod.ResumeLayout(false);
            this.PNL_averagingPeriod.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PNL_measureConfiguration;
        private System.Windows.Forms.Label LBL_chooseMeasureConfiguration;
        private System.Windows.Forms.RadioButton RB_threePhaseMeasure;
        private System.Windows.Forms.RadioButton RB_singlePhaseMeasure;
        private System.Windows.Forms.Button BUT_calculate;
        private System.Windows.Forms.Panel PNL_averagingPeriod;
        private System.Windows.Forms.RadioButton RB_10sec;
        private System.Windows.Forms.RadioButton RB_1min;
        private System.Windows.Forms.RadioButton RB_10mins;
        private System.Windows.Forms.Label label1;
    }
}