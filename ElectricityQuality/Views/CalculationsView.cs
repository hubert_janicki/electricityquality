﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElectricityQuality.Calculators;
using ElectricityQuality.DTOs;

namespace ElectricityQuality.Views
{
    public partial class CalculationsView : Form
    {
        private readonly string _measureFilePath;
        public CalculationsView(string measureFilePath)
        {
            InitializeComponent();

            _measureFilePath = measureFilePath ?? throw new ArgumentNullException(nameof(measureFilePath));
        }

        private void BUT_calculate_Click(object sender, EventArgs e)
        {
            try
            {
                var saveFileDialog = new SaveFileDialog()
                {
                    Filter = @"txt files (*.txt)|*.txt|All files (*.*)|*.*"
                };

                BUT_calculate.Enabled = false;

                int averagePeriod = 0;
                if (RB_10mins.Checked)
                {
                    averagePeriod = 600000;
                }
                else if (RB_1min.Checked)
                {
                    averagePeriod = 60000;
                }
                else
                {
                    averagePeriod = 10000;
                }
                
                if (RB_singlePhaseMeasure.Checked)
                {
                    Log.AddMessage(@"Wybrano zapis jednofazowy");
                    var singlePhaseMeasureEntities = ReadSinglePhaseMeasuresFromFile();

                    var singlePhaseMeasuresCalculator = new SinglePhaseMeasuresCalculator(singlePhaseMeasureEntities, averagePeriod);
                    var results = singlePhaseMeasuresCalculator.Calculate();

                    saveFileDialog.ShowDialog();

                    if(saveFileDialog.FileName != "")
                        singlePhaseMeasuresCalculator.WriteResultsToFile(results, saveFileDialog.FileName);
                }
                else
                {
                    Log.AddMessage(@"Wybrano zapis trojfazowy");
                    var threePhaseMeasureEntities = ReadThreePhaseMeasuresFromFile();

                    var threePhaseMeasuresCalculator = new ThreePhaseMeasuresCalculator(threePhaseMeasureEntities, averagePeriod);
                    var results = threePhaseMeasuresCalculator.Calculate();

                    saveFileDialog.ShowDialog();

                    if (saveFileDialog.FileName != "")
                        threePhaseMeasuresCalculator.WriteResultsToFile(results, saveFileDialog.FileName);

                    var voltageDipsCalculator = new VoltageDipsCalculator(results);
                    var groupedVoltageDips = voltageDipsCalculator.Calculate();

                    saveFileDialog.FileName = "";
                    saveFileDialog.ShowDialog();
                    if (saveFileDialog.FileName != "")
                        voltageDipsCalculator.WriteToFile(groupedVoltageDips, saveFileDialog.FileName, averagePeriod);
                }
                BUT_calculate.Enabled = true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"Error");
                BUT_calculate.Enabled = true;
            }
        }

        private List<SinglePhaseMeasureEntity> ReadSinglePhaseMeasuresFromFile()
        {
            Log.AddMessage(@"Rozpoczeto odczyt z pliku");
            var singlePhaseMeasureEntities = new List<SinglePhaseMeasureEntity>();

            var line = string.Empty;

            var id = 0;
            using (var fileReader = new StreamReader(_measureFilePath))
            {
                while ((line = fileReader.ReadLine()) != null)
                {
                    if(line == string.Empty)
                        continue;
                    
                    var values = line.Split('\t');

                    if (values.Length != 3)
                        throw new InvalidDataException();

                    var singlePhaseMeasureEntity = new SinglePhaseMeasureEntity(
                        ++id,
                        double.Parse(values[0], CultureInfo.InvariantCulture),
                        double.Parse(values[1], CultureInfo.InvariantCulture),
                        double.Parse(values[2], CultureInfo.InvariantCulture)
                        );

                    singlePhaseMeasureEntities.Add(singlePhaseMeasureEntity);
                }
            }

            Log.AddMessage(@"Zakonczono odczyt z pliku");

            return singlePhaseMeasureEntities;
        }

        private ThreePhaseMeasure ReadThreePhaseMeasuresFromFile()
        {
            Log.AddMessage(@"Rozpoczeto odczyt z pliku");
            var L1 = new List<SinglePhaseMeasureEntity>();
            var L2 = new List<SinglePhaseMeasureEntity>();
            var L3 = new List<SinglePhaseMeasureEntity>();

            var line = string.Empty;

            var id = 0;
            using (var fileReader = new StreamReader(_measureFilePath))
            {
                while ((line = fileReader.ReadLine()) != null)
                {
                    var values = line.Split('\t');

                    if (values.Length != 7)
                        throw new InvalidDataException();

                    var timestamp = double.Parse(values[0], CultureInfo.InvariantCulture); //common for three phases

                    id++;
                    L1.Add(new SinglePhaseMeasureEntity(
                        id,
                        timestamp,
                        double.Parse(values[1], CultureInfo.InvariantCulture),
                        double.Parse(values[4], CultureInfo.InvariantCulture)
                    ));

                    L2.Add(new SinglePhaseMeasureEntity(
                        id,
                        timestamp,
                        double.Parse(values[2], CultureInfo.InvariantCulture),
                        double.Parse(values[5], CultureInfo.InvariantCulture)
                    ));

                    L3.Add(new SinglePhaseMeasureEntity(
                        id,
                        timestamp,
                        double.Parse(values[3], CultureInfo.InvariantCulture),
                        double.Parse(values[6], CultureInfo.InvariantCulture)
                    ));
                }
            }

            var threePhaseMeasureEntities = new ThreePhaseMeasure(L1, L2, L3);

            Log.AddMessage(@"Zakonczono odczyt z pliku");

            return threePhaseMeasureEntities;
        }
    }
}
